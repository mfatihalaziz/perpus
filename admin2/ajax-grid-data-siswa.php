<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "smp";

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());
$requestData= $_REQUEST;


$columns = array( 
	0 => 'nis',
    1 => 'nama', 
	2 => 'kelas',
	3 => 'jenis', 
);
$sql = "SELECT nis, nama, kelas, jenis";
$sql.=" FROM siswa";
$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData; 


if( !empty($requestData['search']['value']) ) {
	$sql = "SELECT nis, nama, kelas, jenis ";
	$sql.=" FROM siswa";
	$sql.=" WHERE nama LIKE '%".$requestData['search']['value']."%' ";
	$sql.=" OR nis LIKE '".$requestData['search']['value']."%' ";
	$sql.=" OR kelas LIKE '".$requestData['search']['value']."%' ";
    $sql.=" OR jenis LIKE '".$requestData['search']['value']."%' ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	$totalFiltered = mysqli_num_rows($query); /
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
} else {	
	$sql = "SELECT nis, nama, kelas, jenis ";
	$sql.=" FROM siswa";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	
}

$data = array();
while( $row=mysqli_fetch_array($query) ) {
	$nestedData=array(); 
	$nestedData[] = $row["nis"];
    $nestedData[] = $row["nama"];
	$nestedData[] = $row["kelas"];
	$nestedData[] = $row["jenis"];
    $nestedData[] = '<td><center>
                     <a href="?page=siswa_edit&nis='.$row['nis'].'"  data-toggle="tooltip" title="Edit" class="btn btn-sm btn-warning"> <i class="menu-icon icon-pencil"></i> </a>              
				     </center></td>';		
	
	$data[] = $nestedData;
    
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),    
			"recordsTotal"    => intval( $totalData ),  
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data
			);

echo json_encode($json_data);
?>
