<?php

$servername = "localhost";
$username = "root";
$password = "";
$dbname = "smp";

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());

$requestData= $_REQUEST;


$columns = array( 
	0 => 'id',
    1 => 'judul', 
	2 => 'pengarang',
	3 => 'penerbit',
    4 => 'isbn',
    5 => 'jumlah_buku',  
    6 => 'lokasi'
);

$sql = "SELECT id, judul, pengarang, penerbit, isbn, jumlah_buku, lokasi ";
$sql.=" FROM tbl_buku";
$query=mysqli_query($conn, $sql) or die("ajax-data-buku.php: get InventoryItems");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;


if( !empty($requestData['search']['value']) ) {
	$sql = "SELECT id, judul, pengarang, penerbit, isbn, jumlah_buku, lokasi  ";
	$sql.=" FROM tbl_buku";
	$sql.=" WHERE judul LIKE '%".$requestData['search']['value']."%' ";   
	$sql.=" OR pengarang LIKE '%".$requestData['search']['value']."%' ";
	$sql.=" OR penerbit LIKE '%".$requestData['search']['value']."%' ";
    $sql.=" OR isbn LIKE '%".$requestData['search']['value']."%' ";
    $sql.=" OR lokasi LIKE '%".$requestData['search']['value']."%' ";
	$query=mysqli_query($conn, $sql) or die("ajax-data-buku.php: get PO");
	$totalFiltered = mysqli_num_rows($query); 
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   "; // $requestData['order'][0]['column'] contains colmun index, $requestData['order'][0]['dir'] contains order such as asc/desc , $requestData['start'] contains start row number ,$requestData['length'] contains limit length.
	$query=mysqli_query($conn, $sql) or die("ajax-data-buku.php: get PO"); 
} else {	
	$sql = "SELECT id, judul, pengarang, penerbit, isbn, jumlah_buku, lokasi";
	$sql.=" FROM tbl_buku";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-data-buku.php: get PO");
	
}

$data = array();
while( $row=mysqli_fetch_array($query) ) {
	$nestedData=array(); 
	$nestedData[] = $row["id"];
    $nestedData[] = $row["judul"];
	$nestedData[] = $row["pengarang"];
	$nestedData[] = $row["penerbit"];
    $nestedData[] = $row["isbn"];
    $nestedData[] = $row["jumlah_buku"];
    $nestedData[] = $row["lokasi"];
    $nestedData[] = '<td><center>
                     <a href="?page=buku_edit&kd='.$row['id'].'"  data-toggle="tooltip" title="Edit" class="btn btn-sm btn-warning"> <i class="menu-icon icon-pencil"></i> </a>
                     <a href = "index.php?page=buku_pinjam&kd='.$row['id'].'" data-toggle="tooltip" title="Pinjam" class="btn btn-sm btn-success"> <i class="menu-icon icon-shopping-cart"></i>Pinjam</a></td>';		
	
	$data[] = $nestedData;
    
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),    
			"recordsTotal"    => intval( $totalData ), 
			"recordsFiltered" => intval( $totalFiltered ), 
			"data"            => $data 
			);

echo json_encode($json_data);

?>
