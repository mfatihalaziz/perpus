<?php
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "smp";

$conn = mysqli_connect($servername, $username, $password, $dbname) or die("Connection failed: " . mysqli_connect_error());
  
$requestData= $_REQUEST;


$columns = array( 
	0 => 'idtrx',
	1 => 'id',
    2 => 'judul', 
	3 => 'nis',
	4 => 'nama',
	5 => 'kelas',
	6 => 'tgl_pinjam',
	7 => 'tgl_kembali',
	8 => 'jmlpinjam', 
);

$sql = "SELECT  peminjaman.idtrx, peminjaman.id, peminjaman.judul,siswa.nis, siswa.nama, siswa.kelas,  peminjaman.tgl_pinjam, peminjaman.tgl_kembali, peminjaman.jmlpinjam FROM siswa, peminjaman WHERE siswa.nis=peminjaman.nis";
$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get InventoryItems");
$totalData = mysqli_num_rows($query);
$totalFiltered = $totalData;  


if( !empty($requestData['search']['value']) ) {
	$sql = "SELECT  peminjaman.idtrx, peminjaman.id, peminjaman.judul,siswa.nis, siswa.nama, siswa.kelas,  peminjaman.tgl_pinjam, peminjaman.tgl_kembali, peminjaman.jmlpinjam FROM siswa, peminjaman WHERE siswa.nis=peminjaman.nis";
	$sql.=" OR idtrx LIKE '%".$requestData['search']['value']."%' ";    
	$sql.=" OR judul LIKE '%".$requestData['search']['value']."%' ";

    $sql.=" OR nama LIKE '%".$requestData['search']['value']."%' ";

    $sql.=" OR kelas LIKE '%".$requestData['search']['value']."%' ";
    $sql.=" OR nama LIKE '%".$requestData['search']['value']."%' ";
    $sql.=" OR jmlpinjam LIKE '%".$requestData['search']['value']."%' ";

	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	$totalFiltered = mysqli_num_rows($query); per search result without limit in the query 

	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO"); 
	
} else {	

	$sql = "SELECT  peminjaman.idtrx, peminjaman.id, peminjaman.judul,siswa.nis, siswa.nama, siswa.kelas,  peminjaman.tgl_pinjam, peminjaman.tgl_kembali, peminjaman.jmlpinjam FROM siswa, peminjaman WHERE siswa.nis=peminjaman.nis";
	$sql.=" ORDER BY ". $columns[$requestData['order'][0]['column']]."   ".$requestData['order'][0]['dir']."   LIMIT ".$requestData['start']." ,".$requestData['length']."   ";
	$query=mysqli_query($conn, $sql) or die("ajax-grid-data.php: get PO");
	
}

$data = array();
while( $row=mysqli_fetch_array($query) ) {
	$nestedData=array(); 

	$nestedData[] = $row["idtrx"];
	$nestedData[] = $row["id"];
    $nestedData[] = $row["judul"];
	$nestedData[] = $row["nis"];
	$nestedData[] = $row["nama"];
	$nestedData[] = $row["kelas"];
	$nestedData[] = $row["tgl_pinjam"];
		$nestedData[] = $row["tgl_kembali"];
	$nestedData[] = $row["jmlpinjam"];

    $nestedData[] = '<td><center>
                     <a href="?page=kembalikan_proses&idtrx='.$row['idtrx'].'"  data-toggle="tooltip" title="Edit" class="btn btn-sm btn-warning"> <i class="menu-icon icon-pencil"></i> </a>
                     <a href="?page=siswa_hapus&kd='.$row['nis'].'"  data-toggle="tooltip" title="Edit" class="btn btn-sm btn-danger"> <i class="menu-icon icon-trash"></i> </a>
				     </center></td>';		
	
	$data[] = $nestedData;
    
}



$json_data = array(
			"draw"            => intval( $requestData['draw'] ),
			"recordsTotal"    => intval( $totalData ), 
			"recordsFiltered" => intval( $totalFiltered ),
			"data"            => $data 
			);

echo json_encode($json_data);

?>
